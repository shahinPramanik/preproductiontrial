import re

def split(data):
    
    return re.split(r"(\[\{.*?\}\])",data.replace('\r\n','<br>') )

def getTablehtml(data):
    for m in re.compile(r'\[\{Table(.*?)<br><br>(.*?)\}\]').finditer(data):
        tableStyle=m.group(1)
        tableContent=m.group(2)
    tableCellSplit=tableCol(tableRow(tableContent))
    tableCellSplit=rearragetable(tableCellSplit)
    tableHtml=f"<table table class='table table-bordered' style='{tableStyle}'><tbody>{gethtmlCode(tableCellSplit)}</tbody></table>"
    return tableHtml

def tableRow(data):
    return data.split('<br><br>')

def tableCol(data):
    for row in range(0,len(data)):
        data[row]=data[row].split('<br>')
    return data

def rearragetable(data):
    row=[]
    
    for i in range(0,len(data)):
        col=[]
        for j in range(0,len(data[i])):
            
            if data[i][j].startswith('|'):
                col.append(data[i][j])
            else:
                if len(col)>0:
                    col[len(col)-1]=col[len(col)-1]+'<br>'+data[i][j]     
        row.append(col)
       
    return row

def gethtmlCode(data):
    htmlmarkup=''
    for i in range(0,len(data)):
        tempcell=''
        for j in range(0,len(data[i])):
                style,content=getCellContentStyle(data[i][j])
                print(f'Style:{style}\r\nContent:{content}')
                tempcell=tempcell+f"<td style='{style}'>{content}</td>"
        htmlmarkup=htmlmarkup+f"<tr>{tempcell}</tr>"
    return htmlmarkup

def getCellContentStyle(data):
    style=content=''
    for m1 in re.compile(r'\((.*?)\)(.*)').finditer(data):
        if m1.group(2).startswith(';'):
            for m2 in re.compile(r'(;.*?)\)(.*)').finditer(m1.group(2)):
                style=m1.group(1)+')'+m2.group(1)
                content=m2.group(2) 
        else:
            style=m1.group(1)
            content=m1.group(2)
    return style,content

def markuptoRegularHtmlTags(data):
    data=data.split('<br>')
    for i in range(0,len(data)):
        testData=data[i].split('=')
        print(f'Markup Code {testData[0]}\r\nDescription:{testData[1]}')

def gethtml(data):
    data=split(data)
    htmlmarkup=''
    res=''
    for i in range(0,len(data)):
        if re.match(r'\[\{Table.*?\}\]',data[i]):
            pass
            # res=getTablehtml(data[i])
        elif re.match(r'\[\{MxGraph.*?\}\]',data[i]):
            pass
            # res=f'<pre>MxGraph</pre>'
        else:
            markuptoRegularHtmlTags(data[i])
            res=data[i]
        htmlmarkup=htmlmarkup+res
    return htmlmarkup

