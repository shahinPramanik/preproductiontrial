from flask import Flask, render_template, request,url_for
import json
import markupToHtml 

with open ("sampledata/project.json","r") as f:
    project_data=json.load(f)
with open ("sampledata/tracker_15.json","r") as f:
    project_bsm=json.load(f)
with open ("sampledata/task_tracker_15.json","r") as f:
    task_bsm=json.load(f)
with open ("sampledata/bug_tracker_15.json","r") as f:
    bug_bsm=json.load(f)
with open ("sampledata/US_tracker_15.json","r") as f:
    us_bsm=json.load(f)
with open ("sampledata/tracker_47.json","r") as f:
    project_git_workshop=json.load(f)
with open ("sampledata/task_tracker_47.json","r") as f:
    task_git_workshop=json.load(f)

app = Flask(__name__)

@app.route("/")
def init():
    return render_template("base.html")

@app.route("/project/")
def project():
    # return "{}".format(len(project_data))
    return render_template("projects.html",project=project_data)

@app.route("/bsm/")
def bsm():
    return render_template("tracker.html",
                            tracker=project_bsm,
                            task_item=task_bsm,
                            bug_item=bug_bsm,
                            us_item=us_bsm)

@app.route("/gitWorkshop/")
def gitWorkshop():
    return render_template("tracker.html",
                            tracker=project_git_workshop,
                            task_item=task_git_workshop,
                            bug_item=[],
                            us_item=[],
                            )
@app.route("/wikipage/")
def wikipage():
    with open("sampledata/resutlWiki.json", "r") as f:
        wikires = json.load(f)
    return render_template("wiki.html",
                            wikires=wikires,
                            )

@app.route("/markup/")
def markup():
    with open("sampledata/testDataMarkup.json", "r") as f:
        markupres = json.load(f)
    return render_template("markup.html",
                            markupres=markupres,
                            mrk=markupToHtml
                            )